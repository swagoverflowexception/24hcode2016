package com.swagoverflowexception.lazeroverflow.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.swagoverflowexception.lazeroverflow.MyGdxGame;
import com.swagoverflowexception.lazeroverflow.VrongGame;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new VrongGame(), config);
	}
}
