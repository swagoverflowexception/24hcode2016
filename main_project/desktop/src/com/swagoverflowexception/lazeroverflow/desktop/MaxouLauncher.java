package com.swagoverflowexception.lazeroverflow.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.swagoverflowexception.lazeroverflow.MaxouGame;
import com.swagoverflowexception.lazeroverflow.MyGdxGame;
import com.swagoverflowexception.lazeroverflow.VrongGame;

public class MaxouLauncher
{
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 683;
		config.height = 384;
		new LwjglApplication(new MaxouGame(), config);
	}
}
