package com.swagoverflowexception.lazeroverflow.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.swagoverflowexception.lazeroverflow.MyGdxGame;
import com.swagoverflowexception.lazeroverflow.RaphGame;

public class RaphLauncher
{
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new RaphGame(), config);
	}
}
