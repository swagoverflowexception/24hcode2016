package com.swagoverflowexception.lazeroverflow.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.swagoverflowexception.lazeroverflow.VrongGame;

public class VrongLauncher
{
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 683;
		config.height = 384;
		new LwjglApplication(new VrongGame(), config);
	}
}
