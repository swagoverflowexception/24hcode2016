package com.swagoverflowexception.lazeroverflow.controller;

import com.badlogic.gdx.Gdx;

/**
 * Created by vrong on 24/01/16.
 */
public class TR
{
    public static float translateX(float x, float w)
    {
        float width = Gdx.graphics.getWidth();
        return (x) / width * w;
    }
    public static float translateY(float y, float h)
    {
        float height = Gdx.graphics.getHeight();
        return (height - y) / height * h;
    }
    public static boolean isIn(float xtest, float ytest, float x, float y, float w, float h)
    {
        if(isBetween(x, xtest, x+w) && isBetween(y, ytest, y+h))
            return true;
        else
            return false;
    }
    public static boolean isBetween(float x, float y, float z)
    {
        if(x < y && y < z)
            return true;
        else
            return false;
    }
}
