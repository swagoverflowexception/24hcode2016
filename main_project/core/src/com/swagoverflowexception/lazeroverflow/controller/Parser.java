package com.swagoverflowexception.lazeroverflow.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.swagoverflowexception.lazeroverflow.model.*;
import java.util.ArrayList;

/*

    //Niveau 0 Test
    public static LaserWorld createMonde0(String theme)
    {
        Parser p = new Parser();

        LaserWorld laserWorld = new LaserWorld();
        laserWorld.setMap(p.createLevel(p.getListLevel().get(0)));

        return laserWorld;
    }


 */


/**
 * Created by Quentin on 24/01/2016.
 */
public class Parser
{
    private Bloc[][] blocs;
    private static BlocFactory bf = new BlocFactory();

    public Parser()
    {
        this.blocs = new Bloc[16][9];
    }

    public Bloc[][] createLevel(String nameLevelFile)
    {
        if (Gdx.files.internal(nameLevelFile).exists() && !Gdx.files.internal(nameLevelFile).isDirectory())
        {
            FileHandle fichier = Gdx.files.internal(nameLevelFile);
            String texte = fichier.readString();

            String ligne[] = texte.split(";");

            for (int i=0 ; i<ligne.length ; i++)
            {
                String[] attributs = ligne[i].split("\t");

                if ("Star".equals(attributs[2].trim()))
                    blocs[Integer.parseInt(attributs[0])][Integer.parseInt(attributs[1])] = bf.getStar();
                else if ("Mirror".equals(attributs[2].trim()))
                    blocs[Integer.parseInt(attributs[0])][Integer.parseInt(attributs[1])] = bf.getMirror();
                else if ("RotativeMirror".equals(attributs[2].trim()))
                    blocs[Integer.parseInt(attributs[0])][Integer.parseInt(attributs[1])] = bf.getRotativeMirror();
                else if ("Prism".equals(attributs[2].trim()))
                    blocs[Integer.parseInt(attributs[0])][Integer.parseInt(attributs[1])] = bf.getPrism();
                else if ("RotativePrism".equals(attributs[2].trim()))
                    blocs[Integer.parseInt(attributs[0])][Integer.parseInt(attributs[1])] = bf.getRotativePrism();
                else if ("Receiver".equals(attributs[2].trim()))
                    blocs[Integer.parseInt(attributs[0])][Integer.parseInt(attributs[1])] = bf.getReceiver();
                else if ("Wall".equals(attributs[2].trim()))
                    blocs[Integer.parseInt(attributs[0])][Integer.parseInt(attributs[1])] = bf.getWall();
                else if ("Transmitter".equals(attributs[2].trim()))
                    blocs[Integer.parseInt(attributs[0])][Integer.parseInt(attributs[1])] = bf.getTransmitter();
            }

            fillBloc();

            return this.blocs;
        }
        else
            return null;
    }

    public String saveLevel(Bloc[][] blocs, String nomNiveau)
    {
        FileHandle fichier = Gdx.files.local("data/"+nomNiveau+".txt");

        String texte = "";
        for(int i = 0; i < 16;i++)
            for(int j = 0; j < 9; j++)
            {
                if (!(blocs[i][j] instanceof EmptyBloc) && !(blocs[i][j] instanceof LockBloc))
                {
                    texte += i + "\t" + j + "\t";
                    if (blocs[i][j] instanceof Star)
                        texte += "Star";
                    else if (blocs[i][j] instanceof Receiver)
                        texte += "Receiver";
                    else if (blocs[i][j] instanceof Wall)
                        texte += "Wall";
                    else if (blocs[i][j] instanceof Transmitter)
                        texte += "Transmitter";
                    else if (blocs[i][j] instanceof Prism)
                        if (((Prism) blocs[i][j]).getIn().size() == 2)
                            texte += "Mirror";
                        else
                            texte += "Prism";
                    else if (blocs[i][j] instanceof RotativePrism)
                        if (((Prism) blocs[i][j]).getIn().size() == 2)
                            texte += "RotativeMirror";
                        else
                            texte += "RotativePrism";

                    texte += ";";
                    fichier.writeString(texte, false);
                }
            }

        return "data/" + nomNiveau + ".txt";
    }

    public ArrayList<String> getListLevel()
    {
        ArrayList<String> listLevel = new ArrayList<String>();
        FileHandle[] files = Gdx.files.internal("data").list();
        for(FileHandle file : files)
            listLevel.add(file.path());

        return listLevel;
    }

    private void fillBloc()
    {
        for(int i = 0; i < 16;i++)
            for(int j = 0; j < 9; j++)
            {
                if (blocs[i][j] == null)
                    blocs[i][j] = bf.getEmptyBloc();
                if (i == 0 || i == 15 || j == 8 || j == 0)
                    blocs[i][j] = bf.getLockBloc();
            }
    }

}
