package com.swagoverflowexception.lazeroverflow.model;

import com.badlogic.gdx.Gdx;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;

/**
 * Created   by tertullien on 23/01/16.
 */
public class LaserWorld extends Observable {

    /* Constantes */
    public static final float BLOC_SIZE = 20;
    public static final float LASER_DOT_SIZE = 4;

    Bloc[][] map;
    private String background = "background.png";
    private String themePath = "./sprite/forest/";

    private ArrayList<Laser> displayedLasers = new ArrayList<Laser>();
    private ArrayList<Laser> executingLasers = new ArrayList<Laser>();

    private void laseriserTransmitter(Transmitter trans, float x, float y)
    {
        trans.activate();
        //System.out.println(x +" " + y);
        Quarter q = trans.getOut();
        //System.out.println(q);
        switch(q)
        {
            case WEST:
                y = LaserWorld.BLOC_SIZE/2 - LaserWorld.LASER_DOT_SIZE/2 + y*BLOC_SIZE;
                x = x*BLOC_SIZE;
                break;
            case NORTH:
                x = LaserWorld.BLOC_SIZE/2 - LaserWorld.LASER_DOT_SIZE/2 + x*BLOC_SIZE;
                y = LaserWorld.BLOC_SIZE + y*BLOC_SIZE;
                break;
            case EAST:
                y = LaserWorld.BLOC_SIZE/2 - LaserWorld.LASER_DOT_SIZE/2 + y*BLOC_SIZE;
                x = LaserWorld.BLOC_SIZE + x*BLOC_SIZE;
                break;
            case SOUTH:
                x = LaserWorld.BLOC_SIZE/2 - LaserWorld.LASER_DOT_SIZE/2 + x*BLOC_SIZE;
                y = y*BLOC_SIZE;
                break;

        }
        Laser laser = new Laser(q, x, y, trans);
        displayedLasers.add(laser);
        executingLasers.add(laser);
    }
    private void laseriserPrism(Prism prism, float xo, float yo, Quarter source, ArrayList<Laser> add)
    {
        List<Quarter> outs = prism.getIn();
        for(Quarter q : outs)
        {
            float x = xo, y = yo;
            switch(q)
            {
                case WEST:
                    y = LaserWorld.BLOC_SIZE/2 - LaserWorld.LASER_DOT_SIZE/2 + y*BLOC_SIZE;
                    x = x*BLOC_SIZE;
                    break;
                case NORTH:
                    x = LaserWorld.BLOC_SIZE/2 - LaserWorld.LASER_DOT_SIZE/2 + x*BLOC_SIZE;
                    y = LaserWorld.BLOC_SIZE + y*BLOC_SIZE;
                    break;
                case EAST:
                    y = LaserWorld.BLOC_SIZE/2 - LaserWorld.LASER_DOT_SIZE/2 + y*BLOC_SIZE;
                    x = LaserWorld.BLOC_SIZE + x*BLOC_SIZE;
                    break;
                case SOUTH:
                    x = LaserWorld.BLOC_SIZE/2 - LaserWorld.LASER_DOT_SIZE/2 + x*BLOC_SIZE;
                    y = y*BLOC_SIZE;
                    break;

            }

            if(q != source)
            {
                Laser laser = new Laser(q, x, y, prism);
                add.add(laser);
            }
        }


    }
    boolean start = false;
    public void start()
    {
        reset();
        for(int i = 0; i < getMap().length; i++)
        {
            for(int j = 0; j < getMap()[i].length; j++)
            {
                if(getMap()[i][j] instanceof Transmitter)
                {
                    Transmitter trans = (Transmitter)getMap()[i][j];
                    laseriserTransmitter(trans, i, j);
                }
            }
        }
        start = true;

    }
    public void reset()
    {
        nbEtoiles = 0;
        finish = false;
        finishTaken = false;
        for(int i = 0; i < getMap().length; i++)
        {
            for(int j = 0; j < getMap()[i].length; j++)
            {
                getMap()[i][j].reset();
            }
        }
        executingLasers.clear();
        displayedLasers.clear();
    }

    public void process()
    {
        if(start)
        {
            ArrayList<Laser> deleteLasers = new ArrayList<Laser>();
            ArrayList<Laser> add = new ArrayList<Laser>();

            for(Laser laser : executingLasers)
            {
                Long l = (long)(Gdx.graphics.getDeltaTime() * 1000);
                laser.process(l);

                Pair<Float, Float> coord = laser.calculateDot(laser.getDots()-10);
                float x = coord.getFirst();
                float y =  coord.getSecond();

                Bloc bloc = map[Math.round(x / BLOC_SIZE)][Math.round(y / BLOC_SIZE)];
                Quarter source = Quarter.values()[(laser.getDirection().getValue() + 2) % 4];
                if(!bloc.isActive())
                {
                    if(bloc instanceof Prism && ((Prism)bloc).getIn().contains(source))
                    {
                        laseriserPrism((Prism) bloc, Math.round(x / BLOC_SIZE), Math.round(y / BLOC_SIZE), source, add);
                    }
                }
                bloc.activate();

                if(!(bloc instanceof EmptyBloc) && bloc != laser.getSource() && !(bloc instanceof Star) || bloc instanceof Wall)
                {
                    deleteLasers.add(laser);
                }

            }

            for (Laser l : deleteLasers) {
                executingLasers.remove(l);
            }
            for(Laser l : add)
            {
                executingLasers.add(l);
                displayedLasers.add(l);
            }

            boolean no = false;
            for(int i = 0; i < getMap().length; i++)
            {
                for(int j = 0; j < getMap()[i].length; j++)
                {
                    if(getMap()[i][j] instanceof Receiver)
                    {
                        Receiver r = (Receiver)getMap()[i][j];
                        if(!r.isActive())
                            no = true;
                    }
                }
            }
            if(no)
                return;
            finish = true;
            start = false;
            for(int i = 0; i < getMap().length; i++)
            {
                for(int j = 0; j < getMap()[i].length; j++)
                {
                    if(getMap()[i][j] instanceof Star)
                    {
                        Star r = (Star)getMap()[i][j];
                        if(!r.isActive())
                            nbEtoiles++;
                    }
                }
            }

        }
    }
    public int nbEtoiles = 0;
    public boolean finish = false;
    public boolean finishTaken = true;

    /**
     * Getters et Setters
     */
    public String getThemePath() {
        return themePath;
    }

    public void setThemePath(String themePath) {
        this.themePath = themePath;
    }
    public void setMap(Bloc[][] map) {
        this.map = map;
    }

    public Bloc[][] getMap() {
        return map;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public ArrayList<Laser> getExecutingLasers()
    {
        return executingLasers;
    }

    public ArrayList<Laser> getDisplayedLasers()
    {
        return displayedLasers;
    }
}