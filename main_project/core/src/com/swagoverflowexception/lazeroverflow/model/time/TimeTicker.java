package com.swagoverflowexception.lazeroverflow.model.time;

/**
 * Created by vrong on 23/01/16.
 */
public class TimeTicker
{
    public long counter = 0;
    public long lastTick = 0;
    public long interval;

    public TimeTicker(long interval)
    {
        this.interval = interval;
    }

    public boolean update(long delta)
    {
        this.counter += delta;
        if((counter - lastTick) >= interval)
        {
            lastTick += interval;
            return true;
        }
        else
            return false;
    }

    public boolean update(float delta)
    {
        return update((long)(delta*1000l));
    }

}
