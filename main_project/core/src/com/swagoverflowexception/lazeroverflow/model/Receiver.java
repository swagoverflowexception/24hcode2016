package com.swagoverflowexception.lazeroverflow.model;

import java.util.ArrayList;

/**
 * Created by Quentin on 23/01/2016.
 */
public class Receiver extends Bloc
{
    protected String defaultSprite;
    protected String activeSprite;
    protected ArrayList<Quarter> in = new ArrayList<Quarter>();

    public String getActiveSprite() {
        return activeSprite;
    }

    public void setActiveSprite(String activeSprite) {
        this.activeSprite = activeSprite;
    }

    public String getDefaultSprite() {
        return defaultSprite;
    }

    public void setDefaultSprite(String defaultSprite)
    {
        super.setCurrentSprite(defaultSprite);
        this.defaultSprite = defaultSprite;
    }

    public ArrayList<Quarter> getIn()
    {
        return in;
    }

    public void setIn(ArrayList<Quarter> in)
    {
        this.in = in;
    }

    public ArrayList<Quarter> collides(Quarter q)
    {
        if (in.contains(q))
            setActivate(true);
        return new ArrayList<Quarter>();
    }

    public void activate()
    {
        setActivate(true);
        this.setCurrentSprite(activeSprite);
    }

    public void reset()
    {
        setActivate(false);
        this.setCurrentSprite(defaultSprite);
    }

    public void click(){}
}
