package com.swagoverflowexception.lazeroverflow.model;

import com.swagoverflowexception.lazeroverflow.model.time.TimeTicker;

import java.util.Observable;

/**
 * Created by vrong on 23/01/16.
 */
public class Laser extends Observable
{
    public static final String horizontal = "laserHorizontal.png";
    public static final String vertical = "laserVertical.png";
    private int dots = 0;
    private Bloc source;
    public Bloc getSource()
    {
        return source;
    }

    public int getDots()
    {
        return dots;
    }


    TimeTicker ticker = new TimeTicker(10);

    /**
     * Renvoie true si une dot a été ajoutée, false sinon
     * @return
     */
    public boolean process(long delta)
    {
        if(ticker.update(delta))
        {
            dots ++;
            return true;
        }
        return false;
    }

    public Laser(Quarter q, float x, float y, Bloc source)
    {
        this.x = x;
        this.y = y;
        this.direction = q;
        this.source = source;
    }

    private float x;
    private float y;
    private Quarter direction;

    public Quarter getDirection()
    {
        return direction;
    }

    public float getY()
    {
        return y;
    }
    public void setY(float y)
    {
        this.y = y;
    }

    public float getX()
    {
        return x;
    }
    public void setX(float x)
    {
        this.x = x;
    }

    public Pair<Float, Float> calculateDot(int dot)
    {
        float x = getX();
        float y = getY();

        float boutDeLaser = LaserWorld.LASER_DOT_SIZE * dot / 5;
        switch (this.getDirection())
        {
            case EAST:
                x = this.getX() + boutDeLaser;
                break;
            case WEST:
                x = this.getX() - boutDeLaser - LaserWorld.LASER_DOT_SIZE;
                break;
            case NORTH:
                y = this.getY() + boutDeLaser;
                break;
            case SOUTH:
                y = this.getY() - boutDeLaser - LaserWorld.LASER_DOT_SIZE;
                break;
        }

        Pair<Float, Float> coord = new Pair<Float, Float>(x, y);
        return coord;

    }

    public String getProperSprite()
    {
        String sprite = Laser.horizontal;
        switch (this.getDirection())
        {
            case EAST:
                sprite = Laser.horizontal;
                break;
            case WEST:
                sprite = Laser.horizontal;
                break;
            case NORTH:
                sprite = Laser.vertical;
                break;
            case SOUTH:
                sprite = Laser.vertical;
                break;
        }

        return sprite;

    }

}
