package com.swagoverflowexception.lazeroverflow.model;

import java.util.ArrayList;

/**
 * Created by Quentin on 23/01/2016.
 */
public class Transmitter extends Bloc
{
    protected String defaultSprite;
    protected Quarter out;

    public String getDefaultSprite() {
        return defaultSprite;
    }

    public void setDefaultSprite(String defaultSprite)
    {
        this.defaultSprite = defaultSprite;
        super.setCurrentSprite(this.defaultSprite);
    }

    public Quarter getOut()
    {
        return out;
    }

    public ArrayList<Quarter> collides(Quarter q)
    {
        setActivate(true);
        return null;
    }

    public void activate()
    {
        setActivate(true);
    }

    public void reset()
    {
        setActivate(false);
        this.setCurrentSprite(defaultSprite);
    }

    public void click(){}

    /**
     * Rotate prends en paramètre un int :
     * - positif pour un sens de roation horaire
     * - négatif pour un sens de roation anti-horaire
     * 0 correspond à ne pas faire de rotation 1 rotation horaire de 1, etc ...
     *
     * @param shift
     */
    public void rotate (int shift)
    {
        char a = super.getCurrentSprite().charAt(super.getCurrentSprite().length() - 5);
        int indexSprite = (Integer.parseInt(a + "") + shift) % 4;
        this.setDefaultSprite(defaultSprite.substring(0, defaultSprite.length() - 5) + indexSprite + ".png");

        out = Quarter.values()[(out.getValue() + shift) % 4];
    }

    public void setOut(Quarter out)
    {
        this.out = out;
    }
}
