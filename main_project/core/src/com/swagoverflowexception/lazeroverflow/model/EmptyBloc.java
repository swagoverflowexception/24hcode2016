package com.swagoverflowexception.lazeroverflow.model;

import java.util.ArrayList;

/**
 * Created by Quentin on 23/01/2016.
 */
public class EmptyBloc extends Bloc
{
    public ArrayList<Quarter> collides(Quarter q)
    {
        return null;
    }

    public void activate() {}

    public void reset() {}

    public void click() {}
}
