package com.swagoverflowexception.lazeroverflow.model;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by vrong on 23/01/16.
 */
public abstract class  Bloc extends Observable
{
    private String currentSprite;
    private boolean active = false;
    public abstract ArrayList<Quarter> collides(Quarter q);
    public abstract void activate();
    public abstract void reset();
    public abstract void click();

    public boolean isActive()
    {
        return active;
    }

    public void setActivate(boolean active)
    {
        this.active = active;
    }

    public String getCurrentSprite()
    {
        return currentSprite;
    }

    public void setCurrentSprite(String sprite)
    {
        this.currentSprite = sprite;
        this.notifyObservers();
    }
}