package com.swagoverflowexception.lazeroverflow.model;

/**
 * Created by Quentin on 23/01/2016.
 */
public class BlocFactory
{
    /**
     * De base le miroir est positionné en bas à gauche
     * @return
     */
    public Prism getMirror()
    {
        Prism m = new Prism();
        m.setDefaultSprite("mirror0.png");
        m.setActiveSprite("activeMirror0.png");
        m.getIn().add(Quarter.EAST);
        m.getIn().add(Quarter.SOUTH);
        return m;
    }

    /**
     * De base le miroir est positionné en bas à gauche
     * @return
     */
    public RotativePrism getRotativeMirror()
    {
        RotativePrism m = new RotativePrism();
        m.setDefaultSprite("mirror0.png");
        m.setActiveSprite("activeMirror0.png");
        m.getIn().add(Quarter.WEST);
        m.getIn().add(Quarter.SOUTH);
        return m;
    }

    /**
     * De base le prisme est positionné la tête en bas (3 entrés)
     * @return
     */
    public Prism getPrism()
    {
        Prism m = new Prism();
        m.setDefaultSprite("prism0.png");
        m.setActiveSprite("activePrism0.png");
        m.getIn().add(Quarter.EAST);
        m.getIn().add(Quarter.WEST);
        m.getIn().add(Quarter.SOUTH);
        return m;
    }

    /**
     * De base le prisme est positionné la tête en bas (3 entrés)
     * @return
     */
    public RotativePrism getRotativePrism()
    {
        RotativePrism m = new RotativePrism();
        m.setDefaultSprite("prism0.png");
        m.setActiveSprite("activePrism0.png");
        m.getIn().add(Quarter.EAST);
        m.getIn().add(Quarter.WEST);
        m.getIn().add(Quarter.SOUTH);
        return m;
    }

    public Wall getWall()
    {
        Wall w = new Wall();
        w.setCurrentSprite("tree.png");
        return w;
    }

    public LockBloc getLockBloc()
    {
        LockBloc w = new LockBloc ();
        w.setCurrentSprite("wall.png");
        return w;
    }

    public EmptyBloc getEmptyBloc()
    {
        EmptyBloc eB = new EmptyBloc();
        eB.setCurrentSprite("null.png");
        return eB;
    }

    // TODO
    public Star getStar()
    {
        Star s = new Star();
        s.setDefaultSprite("star.png");
        s.setActiveSprite("activeStar.png");
        return s;
    }

    //TODO
    /**
     * Par défaut la sortie de l'émetteur est en haut
     * @return
     */
    public Transmitter getTransmitter()
    {
        Transmitter t = new Transmitter();
        t.setDefaultSprite("transmitter0.png");
        t.setOut(Quarter.NORTH);
        return t;
    }

    //TODO
    /**
     * Par défaut la sortie du recepteur est en haut
     * @return
     */
    public Receiver getReceiver()
    {
        Receiver r = new Receiver();
        r.setDefaultSprite("receiver.png");
        r.setActiveSprite("receiver.png");
        r.getIn().add(Quarter.NORTH);
        return r;
    }
}
