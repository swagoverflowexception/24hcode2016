package com.swagoverflowexception.lazeroverflow.model;

/**
 * Created by Quentin on 23/01/2016.
 */
public enum Quarter
{
    NORTH(0),
    EAST(1),
    SOUTH(2),
    WEST(3);

    private final int value;

    private Quarter (int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return this.value;
    }
}