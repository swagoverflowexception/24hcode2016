package com.swagoverflowexception.lazeroverflow.model;

import java.util.ArrayList;

/**
 * Created by Quentin on 23/01/2016.
 */
public class RotativePrism extends Prism
{
    public void click()
    {
        this.rotate(1);
    }

    /**
     * Rotate prends en paramètre un int :
     * - positif pour un sens de roation horaire
     * - négatif pour un sens de roation anti-horaire
     * 0 correspond à ne pas faire de rotation 1 rotation horaire de 1, etc ...
     *
     * @param shift
     */
    public void rotate (int shift)
    {
        char a = super.getCurrentSprite().charAt(super.getCurrentSprite().length() - 5);
        int indexSprite = (Integer.parseInt(a + "") + shift) % 4;
        super.setDefaultSprite(super.getDefaultSprite().substring(0, super.getDefaultSprite().length() - 5) + indexSprite + ".png");
        super.setActiveSprite(super.getActiveSprite().substring(0, super.getActiveSprite().length() - 5) + indexSprite + ".png");

        ArrayList<Quarter> newIn = new ArrayList<Quarter>();
        for (Quarter quarter : getIn())
        {

            quarter = Quarter.values()[(quarter.getValue() + shift) % 4];
            newIn.add(quarter);
        }
        setIn(newIn);
    }
}


