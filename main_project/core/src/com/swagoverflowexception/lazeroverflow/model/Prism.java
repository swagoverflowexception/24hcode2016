package com.swagoverflowexception.lazeroverflow.model;

import java.util.ArrayList;

/**
 * Created by Quentin on 23/01/2016.
 */
public class Prism extends Bloc
{
    protected String defaultSprite;
    protected String activeSprite;
    protected ArrayList<Quarter> in = new ArrayList<Quarter>();

    public String getActiveSprite() {
        return activeSprite;
    }

    public void setActiveSprite(String activeSprite) {
        this.activeSprite = activeSprite;
    }

    public String getDefaultSprite() {
        return defaultSprite;
    }

    public void setDefaultSprite(String defaultSprite)
    {
        super.setCurrentSprite(defaultSprite);
        this.defaultSprite = defaultSprite;
    }

    public ArrayList<Quarter> getIn()
    {
        return in;
    }

    public void setIn(ArrayList<Quarter> in) {
        this.in = in;
    }

    public ArrayList<Quarter> collides(Quarter q)
    {
        if (!in.contains(q)) {
            return new ArrayList<Quarter>();
        }
        else {
            setActivate(true);
            ArrayList<Quarter> out = new ArrayList<Quarter>();
            for (Quarter q_in : in)
                if (q.getValue() != q_in.getValue())
                    out.add(q_in);
            return out;
        }
    }

    public void activate()
    {
        setActivate(true);
        this.setCurrentSprite(activeSprite);
    }

    public void reset()
    {
        setActivate(false);
        this.setCurrentSprite(defaultSprite);
    }

    public void click(){}
}
