package com.swagoverflowexception.lazeroverflow.model;

import com.swagoverflowexception.lazeroverflow.controller.Parser;

/**
 * Created by Max on 23/01/16 edited by PE.
 */
public class WorldFactory {


    private static BlocFactory bf = new BlocFactory();
    //Méthode qui initialise le tableau
    private static Bloc[][] init(LaserWorld laserWorld)
    {
        Bloc[][] blocs = new Bloc[16][9];
        for(int i = 0; i < 16;i++)
        {
            for(int j = 0; j < 9; j++){
                blocs[i][j]= bf.getEmptyBloc();
                blocs[i][j] = bf.getEmptyBloc();
            }
        }
        return blocs;
    }

    //Niveau 0 Test
    public static LaserWorld createMonde0(String theme)
    {
        Parser p = new Parser();

        LaserWorld laserWorld = new LaserWorld();
        laserWorld.setMap(p.createLevel(p.getListLevel().get(0)));

        return laserWorld;
    }

    //Niveau 1
    public static LaserWorld createMonde1(String theme)
    {
        bf = new BlocFactory();
        LaserWorld laserWorld = new LaserWorld();
        Bloc[][] blocs = init(laserWorld);

        blocs[11][4] = bf.getReceiver();
        blocs[8][4] = bf.getRotativePrism();
        //blocs[8][4] = bf.getPrism();
        blocs[8][4] = bf.getRotativePrism();
        ((RotativePrism)blocs[8][4]).rotate(2);
        blocs[1][1] = bf.getRotativePrism();
        ((RotativePrism)blocs[8][4]).rotate(2);
        blocs[4][5] = bf.getMirror();
        blocs[7][5] = bf.getWall();
        blocs[4][4] = bf.getTransmitter();
        ((Transmitter)blocs[4][4]).rotate(1);

        Parser p = new Parser();
        p.saveLevel(blocs, "level1");

        laserWorld.setMap(blocs);
        return laserWorld;
    }


    //Niveau 2
    public static LaserWorld createMonde2(String theme)
    {
        bf = new BlocFactory();
        LaserWorld laserWorld = new LaserWorld();
        Bloc[][] blocs = init(laserWorld);

        blocs[2][2] = bf.getTransmitter();
        blocs[5][2] = bf.getStar();
        blocs[7][2] = bf.getRotativeMirror();
        blocs[7][4] = bf.getRotativePrism();
        blocs[5][4] = bf.getRotativePrism();
        blocs[10][4] = bf.getReceiver();
        blocs[9][4] = bf.getStar();
        blocs[2][5] = bf.getStar();

        ((Transmitter)blocs[2][2]).rotate(1);
        ((RotativePrism)blocs[7][2]).rotate(1);
        ((RotativePrism)blocs[7][4]).rotate(3);

        //Cadre d'arbre
        for(int i = 0; i < 9 ;i++)
        {
            for(int j = 0; j < 16; j++)
            {
                if(i == 0 || i == 1  || i == 8 || j == 0 || j > 9 && i != 4)
                {
                    blocs[j][i] = bf.getWall();
                }
            }
        }

        //Pierres
        blocs[2][5] = bf.getLockBloc();
        blocs[7][5] = bf.getLockBloc();
        blocs[3][7] = bf.getLockBloc();
        blocs[8][2] = bf.getLockBloc();
        blocs[9][7] = bf.getLockBloc();

        //Arbres
        blocs[2][7] = bf.getWall();
        blocs[7][6] = bf.getWall();
        blocs[2][3] = bf.getWall();
        blocs[2][3] = bf.getWall();
        blocs[12][1] = bf.getWall();
        blocs[15][7] = bf.getWall();
        blocs[3][8] = bf.getWall();

        Parser p = new Parser();
        p.saveLevel(blocs, "level2");

        laserWorld.setMap(blocs);
        return laserWorld;
    }


    public static LaserWorld createMonde3(String theme)
    {
        bf = new BlocFactory();
        LaserWorld laserWorld = new LaserWorld();
        Bloc[][] blocs = init(laserWorld);

        blocs[1][6] = bf.getTransmitter();
        ((Transmitter)blocs[1][6]).rotate(1);
        blocs[2][6] = bf.getRotativeMirror();
        blocs[2][7] = bf.getRotativeMirror();
        blocs[7][5] = bf.getRotativeMirror();
        blocs[5][5] = bf.getRotativePrism();
        blocs[5][2] = bf.getRotativePrism();
        blocs[7][2] = bf.getRotativeMirror();
        blocs[3][2] = bf.getRotativeMirror();
        blocs[3][1] = bf.getRotativeMirror();
        blocs[1][1] = bf.getRotativeMirror();


        //Cadre d'arbre
        for(int i = 0; i < 9 ;i++)
        {
            for(int j = 0; j < 16; j++)
            {
                if(i == 0 || i == 8 || j == 0 || j == 15)
                {
                    blocs[j][i] = bf.getLockBloc();
                }
            }
        }

        Parser p = new Parser();
        p.saveLevel(blocs, "level3");

        System.out.print("hey");

        laserWorld.setMap(blocs);
        return laserWorld;
    }



}
