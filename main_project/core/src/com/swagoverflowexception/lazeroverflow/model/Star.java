package com.swagoverflowexception.lazeroverflow.model;

import java.util.ArrayList;

/**
 * Created by Quentin on 23/01/2016.
 */
public class Star extends Bloc
{
    protected String defaultSprite;
    protected String activeSprite;

    public String getActiveSprite() {
        return activeSprite;
    }

    public void setActiveSprite(String activeSprite) {
        this.activeSprite = activeSprite;
    }

    public String getDefaultSprite() {
        return defaultSprite;
    }

    public void setDefaultSprite(String defaultSprite)
    {
        super.setCurrentSprite(defaultSprite);
        this.defaultSprite = defaultSprite;
    }

    public ArrayList<Quarter> collides(Quarter q)
    {
        setActivate(true);
        ArrayList<Quarter> out = new ArrayList<Quarter>();
        out.add(Quarter.values()[(q.getValue() + 2) % 4]);
        return out;
    }

    public void activate()
    {
        setActivate(true);
        this.setCurrentSprite(activeSprite);
    }

    public void reset()
    {
        setActivate(false);
        this.setCurrentSprite(defaultSprite);
    }

    public void click(){}
}
