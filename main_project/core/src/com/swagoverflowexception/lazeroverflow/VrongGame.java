package com.swagoverflowexception.lazeroverflow;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.swagoverflowexception.lazeroverflow.model.LaserWorld;
import com.swagoverflowexception.lazeroverflow.model.WorldFactory;
import com.swagoverflowexception.lazeroverflow.view.LeafRenderer;
import com.swagoverflowexception.lazeroverflow.view.MenuRenderer;
import com.swagoverflowexception.lazeroverflow.view.WorldView;

public class VrongGame extends ApplicationAdapter {

	WorldView v;
	LaserWorld w;
	LeafRenderer effects;

	MenuRenderer menu;
	int gamestate = 0;

	@Override
	public void create () {
		menu = new MenuRenderer();
		effects = new LeafRenderer("./sprite/forest/");

		w = WorldFactory.createMonde2("./sprite/forest/");
		v = new WorldView(w);
		v.render();
		w.start();


	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		switch (gamestate)
		{
			case 0:
				gamestate = menu.render();
				break;
			case 1:
				w.process();
				v.render();
				break;
		}
		effects.render();
	}
}
