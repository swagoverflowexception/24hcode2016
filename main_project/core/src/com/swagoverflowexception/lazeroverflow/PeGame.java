package com.swagoverflowexception.lazeroverflow;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.swagoverflowexception.lazeroverflow.model.LaserWorld;
import com.swagoverflowexception.lazeroverflow.model.WorldFactory;
import com.swagoverflowexception.lazeroverflow.view.ClickProcessor;
import com.swagoverflowexception.lazeroverflow.view.WorldView;

public class PeGame extends ApplicationAdapter {

	SpriteBatch batch;
	Texture img;
	WorldView v;
	LaserWorld w;

	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");

		w = WorldFactory.createMonde2("./sprite/forest/");
		v = new WorldView(w);
		v.render();
		w.start();
	}

	@Override
	public void render () {
		w.process();
		v.render();
	}
}
