package com.swagoverflowexception.lazeroverflow.view;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.swagoverflowexception.lazeroverflow.model.Bloc;
import com.swagoverflowexception.lazeroverflow.model.LaserWorld;
import com.swagoverflowexception.lazeroverflow.model.RotativePrism;

/**
 * Created by root on 24/01/16.
 */
public class ClickProcessor extends InputAdapter {

    private LaserWorld laserWorld;

    public ClickProcessor(LaserWorld laserWorld) {
        this.laserWorld = laserWorld;
    }

    @Override
    public boolean touchDown (int screenX, int screenY, int pointer, int button) {

        int x = Math.round(screenX / (Gdx.graphics.getWidth() / 16));
        int y = 8 - Math.round(screenY / (Gdx.graphics.getHeight() / 9));

        if (x >= 0 && x < laserWorld.getMap().length && y >= 0 && y < laserWorld.getMap()[0].length) {
            Bloc bloc = laserWorld.getMap()[x][y];

            if (bloc instanceof RotativePrism) {
                System.out.println("BOUGE");
                if (button == Input.Buttons.LEFT)
                    ((RotativePrism) bloc).rotate(1);
                else if (button == Input.Buttons.RIGHT)
                    ((RotativePrism) bloc).rotate(3);
            }
        }

        return true;
    }

}
