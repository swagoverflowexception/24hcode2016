package com.swagoverflowexception.lazeroverflow.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.swagoverflowexception.lazeroverflow.controller.TR;
import com.swagoverflowexception.lazeroverflow.model.time.TimeTicker;

/**
 * Created by vrong on 24/01/16.
 */
public class MenuRenderer
{
    private OrthographicCamera cam;
    private SpriteBatch batch;

    private float W = 1920;
    private float H = 1080;


    Element title, jouer, editor, author;
    Texture bg;

    public MenuRenderer()
    {
        batch = new SpriteBatch();

        cam = new OrthographicCamera(W, H);
        cam.position.set(W / 2, H / 2, 0);
        cam.update();


        bg = new Texture("./menu/menubg.png");//goal start size time
        title = new Element(new Texture("./menu/title.png"), 200, 600, 200, 1200, 1650, 550, 3000);
        jouer = new Element(new Texture("./menu/jouer.png"), 700, 500, -500, 500, 400, 100, 1000);
        editor = new Element(new Texture("./menu/editor.png"), 700, 300, -500, 300, 400, 100, 1000);
        author = new Element(new Texture("./menu/author.png"), 1200, 10, 1200, -200, 700, 200, 500);

    }

    public int render()
    {
        if(Gdx.input.isTouched())
        {
            int x = Gdx.input.getX();
            int y = Gdx.input.getY();
            float xf = TR.translateX(x, 1920);
            float yf = TR.translateY(y, 1080);
            if(TR.isIn(xf, yf, 700, 500, 400, 100))
            {
                System.out.println(1);
                return 1;
            }
            if(TR.isIn(xf, yf, 700, 300, 400, 100))
            {
                System.out.println(2);
                return 2;
            }
            if(TR.isIn(xf, yf, 1200, 10, 700, 200))
            {
                System.out.println("touched");
            }
        }

        /*int x = Gdx.input.getX();
        int y = Gdx.input.getY();
        float xf = TR.translateX(x, 1920);
        float yf = TR.translateY(y, 1080);
        System.out.println(xf + " " + yf);*/

        cam.update();
        batch.setProjectionMatrix(cam.combined);
        batch.begin();

        batch.draw(bg, 0, 0, 1920, 1080);

                    title.render();
        jouer.render();
        editor.render();
        author.render();

        batch.end();

        return 0;
    }

    private class Element
    {
        Texture texture;
        float   x, y,
                w, h,
                deltax, deltay;
        long nb, nbmax;

        TimeTicker ticker = new TimeTicker(10);

        public Element(Texture text,
                       float goalx, float goaly,
                       float startx, float starty,
                       float w, float h,
                       long time)
        {
            this.x = startx;
            this.y = starty;
            this.w = w;
            this.h = h;
            nbmax = time/10;
            deltax = (goalx - x)/nbmax;
            deltay = (goaly - y)/nbmax;
            nb = 0;
            texture = text;
        }

        public void render()
        {
            if(ticker.update(Gdx.graphics.getDeltaTime()))
            {
                if(nb < nbmax)
                {
                    nb ++;
                    x += deltax;
                    y += deltay;
                }
            }
            batch.draw(texture, x, y, w, h);
        }
    }


}
