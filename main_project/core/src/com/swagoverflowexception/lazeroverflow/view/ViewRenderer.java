package com.swagoverflowexception.lazeroverflow.view;


/**
 * Created by vrong on 11/11/15.
 */
public interface ViewRenderer
{
    public void render();
}
