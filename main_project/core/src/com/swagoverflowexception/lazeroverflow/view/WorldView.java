package com.swagoverflowexception.lazeroverflow.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.swagoverflowexception.lazeroverflow.model.Laser;
import com.swagoverflowexception.lazeroverflow.model.LaserWorld;
import com.swagoverflowexception.lazeroverflow.model.Pair;
import com.swagoverflowexception.lazeroverflow.model.RotativePrism;
import com.swagoverflowexception.lazeroverflow.model.Transmitter;
import com.swagoverflowexception.lazeroverflow.model.time.TimeTicker;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by vrong on 23/01/16.
 */
public class WorldView implements ViewRenderer, Observer
{
    private OrthographicCamera cam;
    private SpriteBatch batch;

    Element bravo, star1, star2;

    private LaserWorld laserWorld;
    public WorldView(LaserWorld laserWorld)
    {

        bravo = new Element(new Texture("./menu/title.png"), -1000, -1000, -1000, -1000, 1650, 550, 3000);
        star1 = new Element(new Texture("./menu/jouer.png"), -1000, -1000, -1000, -1000, 400, 100, 1000);
        star2 = new Element(new Texture("./menu/editor.png"), -1000, -1000, -1000, -1000, 400, 100, 1000);


        this.laserWorld = laserWorld;
        batch = new SpriteBatch();

        cam = new OrthographicCamera((laserWorld.getMap().length /*+1*/) * LaserWorld.BLOC_SIZE,
                (laserWorld.getMap()[0].length /*+ 1*/) * LaserWorld.BLOC_SIZE);
        cam.position.set(getWorldWidth() / 2 /*+ laserWorld.BLOC_SIZE / 2*/, getWorldHeight() / 2 /*+ laserWorld.BLOC_SIZE / 2*/, 0); //le centre git de la caméra
        cam.update();

        ClickProcessor inputProcessor = new ClickProcessor(laserWorld);
        Gdx.input.setInputProcessor(inputProcessor);
    }

    HashMap<String, Texture> textures = new HashMap<String, Texture>();

    public Texture loadTexture(String path)
    {
        Texture texture;
        if((texture = textures.get(path)) != null)
            return texture;
        else {
            texture = new Texture(path);
            textures.put(path, texture);
            return texture;
        }
    }

    @Override
    public void render()
    {
        cam.update();
        batch.setProjectionMatrix(cam.combined);
        batch.begin();
        String nullSprite = laserWorld.getThemePath() + "null.png";


        for(int i = 0 ; i < laserWorld.getMap().length; i++)
        {
            for (int j = 0 ; j < laserWorld.getMap()[i].length; j++)
            {
                batch.draw(loadTexture(nullSprite), i * LaserWorld.BLOC_SIZE, j * LaserWorld.BLOC_SIZE,
                        LaserWorld.BLOC_SIZE, LaserWorld.BLOC_SIZE);

                String sprite = laserWorld.getThemePath() + laserWorld.getMap()[i][j].getCurrentSprite();

                batch.draw(loadTexture(sprite), i*LaserWorld.BLOC_SIZE, j * LaserWorld.BLOC_SIZE,
                    LaserWorld.BLOC_SIZE, LaserWorld.BLOC_SIZE);
            }
        }

        for(Laser laser : laserWorld.getDisplayedLasers())
        {
            for(int i = 0; i < laser.getDots(); i++)
            {
                String sprite = laserWorld.getThemePath();
                Pair<Float, Float> coord = laser.calculateDot(i);
                sprite += laser.getProperSprite();
                batch.draw(loadTexture(sprite), coord.getFirst(), coord.getSecond(),
                        LaserWorld.LASER_DOT_SIZE, LaserWorld.LASER_DOT_SIZE);
            }
        }

        if(laserWorld.finish)
        {
            if(!laserWorld.finishTaken)
            {
                bravo = new Element(new Texture("./sprite/win.png"), 60, 80, 60, 200, 130, 80, 400);
                star1 = new Element(new Texture("./sprite/forest/star.png"), 70, 70, 70, -40, 60, 60, 600);
                star2 = new Element(new Texture("./sprite/forest/star.png"), 110, 70, 110, -40, 60, 60, 700);
                laserWorld.finishTaken = true;
            }
        }

        bravo.render();
        star1.render();
        star2.render();

        batch.end();
    }



    @Override
    public void update(Observable observable, Object o)
    {
        //render();
    }

    public float getWorldWidth()
    {
        return LaserWorld.BLOC_SIZE * laserWorld.getMap().length;
    }

    public float getWorldHeight()
    {
        return LaserWorld.BLOC_SIZE * laserWorld.getMap()[0].length;
    }

    private class Element
    {
        Texture texture;
        float   x, y,
                w, h,
                deltax, deltay;
        long nb, nbmax;

        TimeTicker ticker = new TimeTicker(10);

        public Element(Texture text,
                       float goalx, float goaly,
                       float startx, float starty,
                       float w, float h,
                       long time)
        {
            this.x = startx;
            this.y = starty;
            this.w = w;
            this.h = h;
            nbmax = time/10;
            deltax = (goalx - x)/nbmax;
            deltay = (goaly - y)/nbmax;
            nb = 0;
            texture = text;
        }

        public void render()
        {
            if(ticker.update(Gdx.graphics.getDeltaTime()))
            {
                if(nb < nbmax)
                {
                    nb ++;
                    x += deltax;
                    y += deltay;
                }
            }
            batch.draw(texture, x, y, w, h);
        }
    }
}
