package com.swagoverflowexception.lazeroverflow.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.swagoverflowexception.lazeroverflow.model.time.TimeTicker;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by vrong on 24/01/16.
 */
public class LeafRenderer
{
    private OrthographicCamera cam;
    private SpriteBatch batch;

    private float W = 1920;
    private float H = 1080;

    private ArrayList<Leaf> leaves = new ArrayList<Leaf>();

    private String theme;
    public LeafRenderer(String themePath)
    {
        TextureRegion texture = new TextureRegion(new Texture(themePath + "leaf.png"));
        for(int i = 0; i < 20; i++)
        {
            leaves.add(new Leaf(texture));
        }

        batch = new SpriteBatch();

        cam = new OrthographicCamera(W, H);
        cam.position.set(W/2, H/2, 0);
        cam.update();
        this.theme = themePath;
    }

    public void render()
    {
        for(Leaf l : leaves)
            l.render();
    }

    private class Leaf
    {
        Random r = new Random();
        TextureRegion texture;
        float x, y, w = 240, h = 160, scale = 0.3f, rotation = 0;
        TimeTicker ticker = new TimeTicker(1);

        public Leaf(TextureRegion region)
        {
            x = r.nextFloat()*W;
            y = r.nextFloat()*H;
            this.texture = region;
        }

        public void render()
        {
            if(ticker.update(Gdx.graphics.getDeltaTime()))
            {
                x += r.nextFloat()*6;
                y -= r.nextFloat()*3;
                scale += r.nextFloat() % 0.01 - 0.005;
                rotation += r.nextFloat()* 10;

                if(x > W + 200 )
                {
                    x = 0 - 200;
                }
                if(y < 0 - 200 )
                {
                    y = H + 200;
                }


            }
            cam.update();
            batch.setProjectionMatrix(cam.combined);
            batch.begin();
            batch.draw(texture, x, y,  w*scale,  h*scale, w, h, scale, scale, rotation);
            //batch.draw(texture, x, y, w, h);
            //batch.draw(new Texture("badlogic.jpg"), 0, 0, 1920, 1080);
            batch.end();
        }
    }
}
